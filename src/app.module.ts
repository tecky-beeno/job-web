import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WebModule } from './web/web.module';
import { ScrapModule } from './scrap/scrap.module';
import { DBModule } from './db/db.module';

@Module({
  imports: [WebModule, ScrapModule, DBModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
