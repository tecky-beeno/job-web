export type SelectKeywordByKeywordParameters = {
  keyword: any,
}
export type SelectKeywordByKeywordRow = {
  id: any,
  keyword: any,
}

export type SelectKeywordByIdParameters = {
  id: any,
}
export type SelectKeywordByIdRow = {
  id: any,
  keyword: any,
}
