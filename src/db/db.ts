import DB from 'better-sqlite3-helper';

export const db = DB({
  path: 'data/sqlite3.db',
  migrate: {
    force: 'last',
  },
});
