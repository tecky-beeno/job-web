import { Injectable } from '@nestjs/common';
import { config } from 'dotenv';
import { db } from './db';
import {
  SelectKeywordByIdRow,
  SelectKeywordByKeywordParameters,
  SelectKeywordByKeywordRow,
} from './db.service.types';
import { SqlTypeFile } from 'gen-sql-type';
import { Statement } from 'better-sqlite3';

config();

const sqlTypeFile = SqlTypeFile.withPrefix(__filename);

const selectKeywordByKeyword: Statement = db.prepare(
  sqlTypeFile.wrapSql(
    'SelectKeywordByKeyword',
    `select id, keyword
     from keyword
     where keyword = :keyword`,
  ),
);

const selectKeywordById: Statement = db.prepare(
  sqlTypeFile.wrapSql(
    'SelectKeywordById',
    `select id, keyword
     from keyword
     where id = :id`,
  ),
);

@Injectable()
export class DBService {
  getKeyword(param: SelectKeywordByKeywordParameters) {
    let row = selectKeywordByKeyword.get(param);
    if (row) {
      return row as SelectKeywordByKeywordRow;
    }
    const id = db.insert('keyword', param);
    row = selectKeywordById.get({ id });
    return row as SelectKeywordByIdRow;
  }
}
