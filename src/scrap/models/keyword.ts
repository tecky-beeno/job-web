import { Job } from './job';
export class Keyword {
  id: number;

  keyword: string;

  jobs: Job[];
}
