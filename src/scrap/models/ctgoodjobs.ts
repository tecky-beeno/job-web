import { Keyword } from './keyword';
import { Job } from './job';

export class CTPage {
  id: number;

  keyword: Keyword;

  page: number;

  scrape_date: Date;

  is_last: boolean;
}

export class CTJob {
  id: number;

  page: CTPage;

  title: string;

  job_id: string; // for url

  job: Job;

  ref_no: string;
}
