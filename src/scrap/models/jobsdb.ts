import { Keyword } from './keyword';
import { Job } from './job';

export class JobsDBPage {
  id: number;

  keyword: Keyword;

  page: number;

  created_at: Date;

  is_last: boolean;
}

export class JobsDBPJob {
  id: number;

  page: JobsDBPage;

  title: string;

  job_id: string;

  job: Job;
}
