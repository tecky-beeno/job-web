import { Keyword } from './keyword';

export class Job {
  id: number;

  title: string;

  company: string;

  posted_on: Date;

  desc_html: string;

  full_html: string;

  keywords: Keyword[];

  // career_level;
  // year_of_exp;
  // company_website;
  // qualification;
  // employment_term;
}
