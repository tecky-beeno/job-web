import { Controller, Get, Param } from '@nestjs/common';
import { DBService } from '../db/db.service';

@Controller('scrap')
export class ScrapController {
  constructor(private db: DBService) {
  }

  @Get('/:keyword')
  async getKeyword(@Param('keyword') keyword: string) {
    const keywordRow = this.db.getKeyword({ keyword });
    return keywordRow;
    // const jobResult = await this.db.jobs.insert({
    //   title: 'test job',
    //   company: 'test com',
    //   posted_on: new Date(),
    //   desc_html: 'desc html',
    //   full_html: 'full html',
    //   keywords: [],
    // });
    // await this.db.jobs.save({
    //   id: jobResult.identifiers[0].id,
    //   keywords: [keywordRow],
    // });
    // let count = await this.db.jobs.count();
    // let result = await this.db.keywords.find({relations:['has']})
    // console.log(result)
    // return {
    //   ...keywordRow,
    //   count,
    // };
  }
}
