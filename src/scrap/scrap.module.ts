import { Module } from '@nestjs/common';
import { ScrapController } from './scrap.controller';
import { DBService } from '../db/db.service';
import { DBModule } from '../db/db.module';
import { ScrapService } from './scrap.service';

@Module({
  imports: [DBModule],
  providers: [DBService, ScrapService],
  controllers: [ScrapController],
})
export class ScrapModule {}
