import { Injectable } from '@nestjs/common';
import { SqlTypeFile } from 'gen-sql-type';
import { db } from '../db/db';
import {
  SelectNewJobsDBPage,
  SelectNewKeywordForJobsDBRow,
} from './scrap.service.types';

const sqlTypeFile = SqlTypeFile.withPrefix(__filename);

const selectNewKeywordForJobsDB = db.prepare(
  sqlTypeFile.wrapSql(
    'SelectNewKeywordForJobsDB',
    `
        select keyword.id,
               keyword.keyword
        from keyword
        where keyword.id not in (
            select jobsdb_page.keyword_id
            from jobsdb_page
            where page = 1
              and jobsdb_page.keyword_id = keyword.id
        )
    `,
  ),
);

const selectNewJobsDBPage = db.prepare(
  sqlTypeFile.wrapSql(
    'SelectNewJobsDBPage ',
    `
        select keyword,
               page
        from (select keyword_id,
                     max(page) as page,
                     is_last
              from jobsdb_page
              group by keyword_id)
                 inner join keyword on keyword.id = keyword_id
        where is_last = false
    `,
  ),
);

@Injectable()
export class ScrapService {
  constructor() {
    this.check();
  }

  check() {
    const newKeywordForJobsDB = selectNewKeywordForJobsDB.get();
    if (newKeywordForJobsDB) {
      this.scrapeNewKeywordForJobsDB(newKeywordForJobsDB);
      return;
    }
    const newJobsDBPage = selectNewJobsDBPage.get();
    if (newJobsDBPage) {
      return;
    }
  }

  scrapeNewKeywordForJobsDB(param: SelectNewKeywordForJobsDBRow) {
    console.log('scrapeNewKeywordForJobsDB', param);
  }

  scrapeNewJobsDBPage(param: SelectNewJobsDBPage) {
    console.log('scrapeNewJobsDBPage', param);
  }
}
