export type SelectNewKeywordForJobsDBParameters = {

}
export type SelectNewKeywordForJobsDBRow = {
  id: any,
  keyword: any,
}

export type SelectNewJobsDBPage Parameters = {

}
export type SelectNewJobsDBPage Row = {
  keyword: any,
  page: any,
}
