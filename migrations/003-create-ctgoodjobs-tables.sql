-- Up
create table if not exists ct_page (
    id integer primary key,
    keyword_id integer not null references keyword(id),
    page integer not null,
    create_at integer not null,
    is_last integer not null
);
create table if not exists ct_job (
    id integer primary key,
    page_id integer not null references ct_page(id),
    title text not null,
    -- for url
    ctjob_id text not null,
    job_id integer not null references job(id)
);
-- Down
drop table if not exists ct_job;
drop table if not exists ct_page;
