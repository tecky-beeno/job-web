-- Up
delete from keyword where true;
delete from jobsdb_page where true;
insert into keyword  (id, keyword) VALUES
(1,'nodejs'),
(2,'erlang');
insert into jobsdb_page (id, keyword_id, page, create_at, is_last) VALUES
-- nodejs
(1,1,1,1,0),
(2,1,2,1,0),
(3,1,3,1,0),
(4,1,4,1,1),
-- erlang
(5,2,1,1,0),
(6,2,2,1,0),
(7,2,3,1,0);


-- Down
delete from jobsdb_page where true;
delete from keyword where true;
