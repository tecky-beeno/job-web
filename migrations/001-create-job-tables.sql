-- Up
create table if not exists keyword (
    id integer primary key,
    keyword text not null unique
);
create table if not exists job (
    id integer primary key,
    title text not null,
    company text not null,
    posted_on integer not null,
    desc_html text not null,
    full_html text not null
);
create table if not exists job_keyword (
    job_id integer not null references job(id),
    keyword_id integer not null references keyword(id)
);
-- Down
drop table if exists job_keyword;
drop table if exists job;
drop table if exists keyword;
