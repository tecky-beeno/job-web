-- Up
create table if not exists jobsdb_page (
    id integer primary key,
    keyword_id integer not null references keyword(id),
    page integer not null,
    create_at integer not null,
    is_last integer not null
);
create table if not exists jobsdb_job (
    id integer primary key,
    page_id integer not null references jobsdb_page(id),
    title text not null,
    jobdb_id text not null,
    job_id integer not null references job(id)
);
-- Down
drop table if not exists jobsdb_job;
drop table if not exists jobsdb_page;
